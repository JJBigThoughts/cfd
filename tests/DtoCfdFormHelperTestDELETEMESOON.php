<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;


require_once(__DIR__ . '/../DtoCfd.php');
require_once(__DIR__ . '/../DtoCfdFormHelper.php');


trait beAPerson  {
    /** @var string|null */
    public $FirstName;
    /** @var string|null*/
    public $LastName;
    #/** @var boolean */
    #public $doesHave_FirstName;
    #/** @var boolean*/
    #public $doesHave_LastName;
}

class DtoPersonAttendee  extends \Framework19\Cfd\DtoCfd {
    use beAPerson;
}

class DtoPersonBooker  extends \Framework19\Cfd\DtoCfd  {
    use beAPerson;

    /** @var boolean */
    public $doesHave_Email;

    /** @var string */
    public $Email;

    /** @var boolean */
    public $doesHave_Phone;

    /** @var string|null */
    public $Phone;
}


final class TestDtoCfdFormHelper_GetForm_InputFields extends TestCase {
    function assertArrayValuesMatch(array $arrExpected, array $arrGot, string $ErrorPrefix = '')
    {

        $arrDiff = array_diff($arrExpected, $arrGot);
        $strDiff = implode(", ", $arrDiff);
        $strExpected = implode(", ", $arrExpected);
        $strGot = implode(", ", $arrGot);
        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so missing  $strDiff)");

        $arrDiff = array_diff($arrGot, $arrExpected);
        $strDiff = implode(", ", $arrDiff);
        $strExpected = implode(", ", $arrExpected);
        $strGot = implode(", ", $arrGot);
        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so have extras $strDiff)");
    }

    function test_GetForm_InputFields() {
        $realInputFriendlyNames = ['FirstName','LastName'];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['FirstName','LastName'];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['FirstName','LastName', 'Email', 'Phone'];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames(DtoPersonBooker::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

    }

      function test_GetForm_InputFields_ThatAreRequired() {
        $realInputFriendlyNames = [];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = [];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonAttendee::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");

        $realInputFriendlyNames = ['Email'];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonBooker::class);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");



        $realInputFriendlyNames = ['Phone', 'FirstName', 'Email'];
        $extraNamesWeAreForcingToBeRequired = ['FirstName', 'Phone'];
        $foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_InputFieldsNames_ThatAreRequired(DtoPersonBooker::class, $extraNamesWeAreForcingToBeRequired);
        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
    }


}