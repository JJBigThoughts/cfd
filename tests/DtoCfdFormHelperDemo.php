<?php

declare(strict_types=1);




#require_once(__DIR__ . '/../DtoCfd.php');
#equire_once(__DIR__ . '/../DtoCfdFormHelper.php');


trait DtoPerson  {
    /** @var string|null */
    public $FirstName;
    /** @var string|null*/
    public $LastName;
    #/** @var boolean */
    #public $doesHave_FirstName;
    #/** @var boolean*/
    #public $doesHave_LastName;
}

class DtoPersonAttendee  extends \Framework19\Cfd\DtoCfd {
    use DtoPerson;
}

class DtoPersonBooker  extends \Framework19\Cfd\DtoCfd  {
    use DtoPerson;

    #/** @var boolean */
    #public $doesHave_Email;

    /** @var string|null */
    public $Email;

    #/** @var boolean */
    #public $doesHave_Phone;

    /** @var string|null */
    public $Phone;
}

$foundNames = \Framework19\Cfd\DtoCfdFormHelper::GetForm_RichInputFields(DtoPerson::class);
//
//
//final class TestDtoCfdFormHelper_GetForm_InputFields extends TestCase {
//    function assertArrayValuesMatch(array $arrExpected, array $arrGot, string $ErrorPrefix = '')
//    {
//
//        $arrDiff = array_diff($arrExpected, $arrGot);
//        $strDiff = implode(", ", $arrDiff);
//        $strExpected = implode(", ", $arrExpected);
//        $strGot = implode(", ", $arrGot);
//        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so missing  $strDiff)");
//
//        $arrDiff = array_diff($arrGot, $arrExpected);
//        $strDiff = implode(", ", $arrDiff);
//        $strExpected = implode(", ", $arrExpected);
//        $strGot = implode(", ", $arrGot);
//        $this->assertTrue(count($arrDiff) == 0, "$ErrorPrefix Expected: $strExpected, Got: $strGot (so have extras $strDiff)");
//    }
//
//    function test_GetForm_InputFields() {
//        $realInputFriendlyNames = ['FirstName','LastName'];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields(DtoPerson::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//        $realInputFriendlyNames = ['FirstName','LastName'];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields(DtoPersonAttendee::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//        $realInputFriendlyNames = ['FirstName','LastName', 'Email', 'Phone'];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields(DtoPersonBooker::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//    }
//
//      function test_GetForm_InputFields_ThatAreRequired() {
//        $realInputFriendlyNames = [];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields_ThatAreRequired(DtoPerson::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//        $realInputFriendlyNames = [];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields_ThatAreRequired(DtoPersonAttendee::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//        $realInputFriendlyNames = [];
//        $foundNames = \src\Cfd\CfdFormHelper::GetForm_InputFields_ThatAreRequired(DtoPersonBooker::class);
//        $this->assertArrayValuesMatch($realInputFriendlyNames,$foundNames, "Doh");
//
//    }
//
//
//}