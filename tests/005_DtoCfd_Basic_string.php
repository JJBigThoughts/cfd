<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;




class DtoCfdString extends \Framework19\Cfd\DtoCfd {
    /** @var string */
    public $val;
}

class DtoCfdString_optional extends \Framework19\Cfd\DtoCfd {
    /** @var string|null */
    public $val;
}

class DtoCfdString_optionalOrder extends \Framework19\Cfd\DtoCfd {
    /** @var null|string */
    public $val;
}



abstract class TestDtoCfd_BaseString extends TestCase {
     static $CfdName = 'TBD';
     static $ValName = 'TBD';
     static $isNullAnOption = 'TBD';

      function testPreValidationsSubmission_byProperty()
      {
          $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, 1, null);
            $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this: ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

            $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, "1", null);
            $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this: ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

      }
      function testPreValidationsSubmission_byProperty_null()
      {
            $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, null, null);
            $this->assertTrue($dtoValueValidation->isValid == static::$isNullAnOption, "Should not see this: ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);


            // force it to be required
            $dtoValueValidation = static::$CfdName::preValidateProperty(static::$ValName, null, true);
            $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this: ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);
      }


     function testPreValidationsSubmission_easy() {
        $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => 1]);
        $this->assertTrue($dtoValueValidation->isValid == false, "Should not see this: ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

        $dtoValueValidation =  static::$CfdName::preValidateSubmission([static::$ValName => "Hello"]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this:  ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

        $dtoValueValidation =  static::$CfdName::preValidateSubmission([static::$ValName => "1"]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this:  ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

        $dtoValueValidation =  static::$CfdName::preValidateSubmission([static::$ValName => ""]);
        $this->assertTrue($dtoValueValidation->isValid == true, "Should not see this:  ".get_called_class()." dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);

    }

     function testPreValidationsSubmission_null()
     {
             $dtoValueValidation = static::$CfdName::preValidateSubmission([static::$ValName => null]);
             $this->assertTrue($dtoValueValidation->isValid == static::$isNullAnOption, "Should not see this:  " . get_called_class() . " dtoValueValidation({$dtoValueValidation->enumReason}). " . __LINE__);
     }
}

final class TestDtoCfd_Submission_OneVal extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString';
    static $ValName = 'val';
    static $isNullAnOption = false;
}
final class TestDtoCfd_Submission_OneVal_orNull extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString_optional';
    static $ValName = 'val';
    static $isNullAnOption = true;
}
final class TestDtoCfd_Submission_OneVal_orNullReordered extends TestDtoCfd_BaseString
{
    static $CfdName = 'DtoCfdString_optionalOrder';
    static $ValName = 'val';
    static $isNullAnOption = true;
}
