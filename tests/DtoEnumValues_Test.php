<?php
declare(strict_types=1);
namespace testworld;
#require_once(__DIR__ . '/../../../vendor/autoload.php');

use Framework19\Cfd\DtoEnumValues;
use PHPUnit\Framework\TestCase;


class DtoEnumPhase2 extends DtoEnumValues {
    /** @var array */
    public $EnumValues;
    public static $_ArrEnumValuePossibilities = ['Draft', 'RollOut', 'LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];
}

final class TestDtoEnumPhase2 extends TestCase {

    function testBasics() {
        $obj = new DtoEnumPhase2(['EnumValues' => ['Draft']]);
        $this->assertTrue(isset($obj), "Good");
        $this->assertTrue($obj->EnumValues[0] == 'Draft', "Good");

        try {
            $obj = new \testworld\DtoEnumPhase2(['EnumValues' => 'Explosion']);
            $this->assertTrue(0, "Should not get this far cuz not an array");
        } catch (\TypeError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected cuz tried passing an value instead of an array");
        }

        try {
            $obj = new \testworld\DtoEnumPhase2(['EnumValues' => ['Explosion']]);
            $this->assertTrue(0, "Should not get this far cuz not an valid type");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }

    function testBasics2() {
        $obj = new DtoEnumPhase2(['EnumValues' => ['Draft', 'OnOrbit']]);
        $this->assertTrue($obj->EnumValues[0] == 'Draft', "Good");
        $this->assertTrue($obj->EnumValues[1] == 'OnOrbit', "OnOrbit");


        try {
            $obj = new DtoEnumPhase2(['EnumValues' => ['Draft', 'Explosion']]);
            $this->assertTrue(0, "Should not get this far cuz Explosion is a valid value");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }
}
