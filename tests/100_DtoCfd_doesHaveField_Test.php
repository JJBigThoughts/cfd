<?php
declare(strict_types=1);

namespace testworld;

use PHPUnit\Framework\TestCase;

#use \EtFramework19\Pages;

#
#require_once(__DIR__ . '/../../../vendor/autoload.php');


class DtoDummyMissingProperty extends \Framework19\Cfd\DtoCfd
{
    /** @var integer */
    public $SomeInt;

    /** @var boolean */
    public $doesHave_PrettyVersion;
}

class DtoDummyWrongControllerType extends \Framework19\Cfd\DtoCfd
{
    /** @var integer */
    public $SomeInt;

    /** @var integer */
    public $doesHave_PrettyVersion;
}


class DtoDummy_hasReferencedProperty extends \Framework19\Cfd\DtoCfd
{
    /** @var integer */
    public $SomeInt;

    /** @var boolean */
    public $doesHave_PrettyVersion;

    /** @var string */
    public $PrettyVersion; // like 'one', 'six'  But only if 'hasPretty' is set to true. It MUST be null if false

}


class DtoDummyWithConditionalOptional extends \Framework19\Cfd\DtoCfd
{
    /** @var integer */
    public $SomeInt;

    /** @var boolean */
    public $doesHave_PrettyVersion;

    /** @var $string |null */
    public $PrettyVersion = ''; // like 'one', 'six'  But only if 'hasPretty' is set to true. It MUST be null if false

}

final class TestDtoCfd_DtoDummyWithConditionalREq extends TestCase
{
    function testMissingProperty()
    {

        try {
            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt' => 1]);
            $this->assertTrue(0, "Should not get this far. PrettyVersion should be a property..");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true]);
            $this->assertTrue(0, "Should not get this far cuz , even if true, PrettyVersion still needs to be a property. FYI: We don't do defaults yet.");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => false]);
            $this->assertTrue(0, "Should not get this far cuz , even if false, PrettyVersion still needs to be a property. FYI: We don't do defaults yet.");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }

    function testWrongTypeFor_doesHaveField()
    {
        try {
            $obj = new \testworld\DtoDummyWrongControllerType(['SomeInt' => 1, 'doesHave_PrettyVersion' => 1]);
            $this->assertTrue(0, "Should not get this far cuz the controlling property muss be a bool");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }

    function testReferencedProperty_isNotSet()
    {
        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1]);
            $this->assertTrue(0, "Should not get this far cuz we need to set the controllingProperty");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }


        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => 25]);
            $this->assertTrue(0, "Should not get this far cuz 25 isn't the required type, which is bool.");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }
    }

    function testControllingProperty_isSet()
    {
        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true]);
            $this->assertTrue(0, "Should not get this far ");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => false]);
            $this->assertTrue(0, "Should not get this far ");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }
    }

    function testControllingProperty_isSetToTrue()
    {
        $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true, 'PrettyVersion' => "One"]);
        $this->assertTrue(isset($obj), "Good");

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 2, 'doesHave_PrettyVersion' => true]);
            $this->assertTrue(0, "Should not get here");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 3, 'doesHave_PrettyVersion' => true, 'PrettyVersion' => null]);
            $this->assertTrue(0, "Should not get here");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

    }


    function testControllingProperty_isSetToFalse()
    {

        // the other must be null
        $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true, 'PrettyVersion' => "uno"]);
        $this->assertTrue(isset($obj), "Good");

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => false, 'PrettyVersion' => null]);
            $this->assertTrue(isset($obj), "Should not get here cuz Pretty version isn't optional");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true, 'PrettyVersion' => null]);
            $this->assertTrue(!isset($obj), "Should not get this far ");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }

        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => true]);
            $this->assertTrue(!isset($obj), "Should not get this far ");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }
        try {
            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt' => 1, 'doesHave_PrettyVersion' => false]);
            $this->assertTrue(!isset($obj), "Should not get this far ");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that faiiled as expected");
        }



//        try {
//            $obj = new \testworld\DtoDummy_hasReferencedProperty(['SomeInt'=>1,  'doesHave_PrettyVersion' => false , 'PrettyVersion'=>'One']);
//            $this->assertTrue(0, "Should not get this far cuz the controlling property must evaluate to true-ish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }


    }

//    function testBasics() {
//
//        try {
//            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt'=>1]);
//            $this->assertTrue(0, "Should not get this far. PrettyVersion should be a property..");
//        } catch (\Spatie\DataTransferObject\DataTransferObjectError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//        try {
//            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt'=>1,  'doesHave_PrettyVersion' => true ]);
//            $this->assertTrue(0, "Should not get this far cuz , even if true, PrettyVersion still needs to be a property. FYI: We don't do defaults yet.");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//        try {
//            $obj = new \testworld\DtoDummyMissingProperty(['SomeInt'=>1,  'doesHave_PrettyVersion' => false ]);
//            $this->assertTrue(0, "Should not get this far cuz , even if false, PrettyVersion still needs to be a property. FYI: We don't do defaults yet.");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//    }
//    function testLogicValueNotOptional() {
//        try {
//            $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt' => 1, 'doesHave_PrettyVersion' => true]);
//            $this->assertTrue(0, "Should not get this far. If false, we still need to pass a false-ish value");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt' => 1, 'doesHave_PrettyVersion' => 25]);
//        $this->assertTrue(0, "Should not get this far. If false, we still need to pass a false-ish value");
//    }


//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>1]);
//        $this->assertTrue(0, "");

//
//        try {
//            $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => 25, 'PrettyVersion'=>1]);
//            $this->assertTrue(0, "Should not get this far. Controller Vars must be bool, and nothing else");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }


//
//        try {
//            $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>1]);
//            $this->assertTrue(0, "Should not get this far. If true, we still need to pass a non-null");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }

//
//        try {
//            $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false]);
//            $this->assertTrue(0, "Should not get this far. If false, we still need to pass a false-ish value");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>null]);
//        $this->assertTrue(isset($obj), "");
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>0]);
//        $this->assertTrue(isset($obj), "");
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>false]);
//        $this->assertTrue(isset($obj), "");
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>'']);
//        $this->assertTrue(isset($obj), "");
//
//        $obj = new \testworld\DtoDummyWithConditionalNotOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>' ']);
//        $this->assertTrue(isset($obj), "");
//    }
//
//    function testLogicValueOptional() {
//        $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>'']);
//        $this->assertTrue(isset($obj), "");
//
//        $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => false, 'PrettyVersion'=>'   ']);
//        $this->assertTrue(isset($obj), "");
//
//        try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true]);
//            $this->assertTrue(0, "Should not get this far. If true, we need PrettyVersion passed");
//        } catch (\Spatie\DataTransferObject\DataTransferObjectError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//          try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>null]);
//            $this->assertTrue(0, "Should not get this far cuz If true, we need PrettyVersion passed and set to something trueish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//          try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>false]);
//            $this->assertTrue(0, "Should not get this far cuz If true, we need PrettyVersion passed and set to something trueish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//
//          try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>'']);
//            $this->assertTrue(0, "Should not get this far cuz If true, we need PrettyVersion passed and set to something trueish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//          try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>'    ']);
//            $this->assertTrue(0, "Should not get this far cuz If true, we need PrettyVersion passed and set to something trueish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//      try {
//            $obj = new \testworld\DtoDummyWithConditionalOptional(['SomeInt'=>1, 'doesHave_PrettyVersion' => true, 'PrettyVersion'=>0]);
//            $this->assertTrue(0, "Should not get this far cuz If true, we need PrettyVersion passed and set to something trueish");
//        } catch (\src\Cfd\DtoCfdError $e) {
//            $this->assertTrue(true, "Good - that faiiled as expected");
//        }
//
//    }

}