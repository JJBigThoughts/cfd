<?php
declare(strict_types=1);
namespace testworld;



use Framework19\Cfd\DtoEnumValue;
use PHPUnit\Framework\TestCase;

#require_once(__DIR__ . '/../../../vendor/autoload.php');
class DtoCfdInt extends \Framework19\Cfd\DtoCfd {
    /** @var int */
    public $val;
}

class DtoEnumPhaseSimple extends DtoEnumValue {
    /** @var string */
    public $EnumValue;
    public static $_ArrEnumValuePossibilities = ['Draft', 'RollOut', 'LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];

}

class DtoEnumPhase extends \Framework19\Cfd\DtoEnumValue {
    /** @var string */
    public $EnumValue;
    public static $bob = 'hi';
    public static $_ArrEnumValuePossibilities = ['Draft','RollOut','LaunchPad', 'OnOrbit', 'Descent', 'Museum', 'Trash'];
}

class DtoEnumSmallPrimes extends DtoEnumValue {
    /** @var integer */
    public $EnumValue;
    public static $_ArrEnumValuePossibilities = [1,2,3,5,7];
}


final class TestDtoEnumPhase1 extends TestCase {

    function testBasics() {
        $obj = new DtoEnumPhaseSimple(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");

    }

        function testBasics2() {
        $obj = new DtoEnumPhase(['EnumValue' => 'Draft']);
        $this->assertTrue($obj->EnumValue == 'Draft', "Good");


        try {
            $obj = new \testworld\DtoEnumPhase(['EnumValue' => 'Explosion']);
            $this->assertTrue(0, "Should not get this far");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }

    function testDtoEnumSmallPrimes() {
        $obj = new DtoEnumSmallPrimes(['EnumValue' => 1]);
        $this->assertTrue($obj->EnumValue == 1, "Good");


        try {
            $obj = new \testworld\DtoEnumSmallPrimes(['EnumValue' => 4]);
            $this->assertTrue(0, "Should not get this far");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }



         try {
            $obj = new \testworld\DtoEnumSmallPrimes(['EnumValue' => 6]);
            $this->assertTrue(0, "Should not get this far cuz a string");
        } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

         try {
            $obj = new \testworld\DtoEnumSmallPrimes(['EnumValue' => '3']);
            $this->assertTrue(0, "Should not get this far");
         } catch (\Framework19\Cfd\DtoCfdError $e) {
            $this->assertTrue(true, "Good - that failed as expected");
        }

    }
}
