<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



//require_once(__DIR__ . '/../DtoValueValidation.php');



final class TestDtoCfd extends TestCase {


    function testBad()
    {

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation();
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(1);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation([]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => null]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => "Hello"]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }


    }

    function testAlmostValide()
    {

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => "2"]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => "1"]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => ""]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => "0"]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

         try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => 1]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }

        try {
            $obj = new \Framework19\Cfd\DtoValueValidation(['isValid' => 0]);
            $this->assertTrue(0, "Should not get this far. " . __LINE__);
        } catch (TypeError $e) {
            $this->assertTrue(true, "1Good - that faiiled as expected");
        }
    }

    function testVeryValid () {

        $obj = new \Framework19\Cfd\DtoValueValidation(['isValid'=>true]);
        $this->assertTrue($obj->isValid == true, "Should not get this far. ".__LINE__);


        $obj = new \Framework19\Cfd\DtoValueValidation(['isValid'=>false]);
        $this->assertTrue($obj->isValid == false, "Should not get this far. ".__LINE__);


    }


}