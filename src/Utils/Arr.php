<?php

namespace Framework19\Utils;

class Arr
{
    static public function square(int $number): int
    {
        return $number * $number;
    }
}