<?php
// 12/19' Should be moved to Library
namespace Framework19\Cfd;

#require_once(__DIR__ . '/DtoCfd.php');
#use Framework19\Cfd\DtoValueValidation;

class DtoDateTime extends \Framework19\Cfd\DtoCfd {
    /** @var string */
    public $DbtDateTime;

    public static function DbtDateTime_Validates($maybeValidValue) : \Framework19\Cfd\DtoValueValidation {
        $format = 'Y-m-d H:i:s';
//        return false;

        // Handle when they don't put anything after the day. 2000-11-04 would otherwise work

        $t = date($format,strtotime($maybeValidValue));
//        print "<br>time $maybeValidValue => $t";
//        exit;

        if ($maybeValidValue == $t) {
            return new DtoValueValidation(['isValid' => true]);
        } else {
            return new DtoValueValidation(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue Please pass data as exaclty $format " ]);
        }
    }

    public static function now_asString() : string
    {
        return date('Y-m-d H:i:s');//1970-11-04 13:11:25
    }
}




