<?php

namespace Framework19\Cfd;

//use Framework19\Cfd\DtoCfd;
//use Framework19\Cfd\DtoCfdError;

#require_once(__DIR__ . '/property_EnumValue_Validates.php');

abstract class DtoEnumValues extends DtoCfd
{
    /** @var YourTypeGoesHereWhenYouSubClassThis_IfSeeingThis_ThenCopyThisLineAndNextWithRightTtype */
    public $EnumValues;

    use property_EnumValue_Validates;

    public static function EnumValues_Validates(array $maybeValidItems): \Framework19\Cfd\DtoValueValidation
    {

        foreach ($maybeValidItems as $maybeValidItem) {
            $dtoValid = static::EnumValue_Validates($maybeValidItem);
            if ($dtoValid->isValid == false) {
                return $dtoValid;
            }
        }
        return new \Framework19\Cfd\DtoValueValidation(['isValid' => true]);

    }
}
