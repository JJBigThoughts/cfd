<?php
// 12/19' Should be moved to Library
namespace Framework19\Cfd;

#require_once(__DIR__ . '/DtoCfd.php');
#use Framework19\Cfd\DtoValueValidation;

class DtoYmd extends \Framework19\Cfd\DtoCfd {
    /** @var string */
    public $Ymd;

    public static function Ymd_Validates($maybeValidValue) : \Framework19\Cfd\DtoValueValidation {
        $t = date("Y-m-d",strtotime($maybeValidValue)); // https://xkcd.com/1179/

        if ($maybeValidValue == $t) {
            return new DtoValueValidation(['isValid' => true]);
        } else {
            return new DtoValueValidation(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue" ]);
        }
    }
}

