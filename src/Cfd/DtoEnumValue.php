<?php

namespace Framework19\Cfd;

#use Framework19\Cfd\DtoCfd;

#require_once (__DIR__ . '/property_EnumValue_Validates.php');

abstract class DtoEnumValue extends DtoCfd {
    /** @var YourTypeGoesHereWhenYouSubClassThis_IfSeeingThis_ThenCopyThisLineAndNextWithRightTtype */
    public $EnumValue;

    use property_EnumValue_Validates;
}

