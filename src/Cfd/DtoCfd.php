<?php

namespace Framework19\Cfd;
#require_once(__DIR__ . '/DtoValueValidation.php');

use TypeError;
use ReflectionClass;
use ReflectionProperty;
#use Framework19\Cfd\DtoValueValidation;

/* 1/22' KnownBugOfOmmission - this isn't checking for missing parameters */
abstract class DtoCfd //extends \Spatie\DataTransferObject\DataTransferObject
{

    public const FORBIDDEN_TYPES = ['int', 'bool']; // mainly to avoid confusion
    protected const  META_PREFIXES = ['doesHave'];
    #protected const  META_PROPERTY_NAMES = ['_ArrEnumValuePossibilities', 'fieldOrder'];
    protected const  META_PROPERTY_NAMES = [
        '_ArrEnumValuePossibilities',

    ];

    public static function deriveShortNameFromCfdClassName() : string
    {
        $fqn = get_called_class();
        return static::deriveShortNameFromCfdClassName_givenFqn($fqn);
    }

    public static function deriveShortNameFromCfdClassName_givenFqn(string $fqn) : string
    {
         $arrFqn = explode('\\',$fqn);
         $shortTableName = array_pop($arrFqn);
         $Words_withoutHungarian = \EtStringConvert::camelCaseToEnglish($shortTableName, ['dto','Cfd','cfdb','cfe','cftbl','tbl']);
         $shortTableName = implode(explode(' ', $Words_withoutHungarian));
         return $shortTableName;
    }


    private static function StartsWith(string $prefix, string $longString): bool
    { //https://www.geeksforgeeks.org/php-startswith-and-endswith-functions/
        $len = mb_strlen($prefix);
        return (mb_substr($longString, 0, $len) === $prefix);
    }

    private static function TrimOffFront(string $prefix, string $longString): string
    {
        $len = mb_strlen($prefix);
        return mb_substr($longString, $len);
    }

    /* Given an array of dto items, and a key, return an array
    class DtoTab {
         public $Slug;
        public $Text
    }
    $arrTabs = [
        new DtoTab(['Slug'=>'Merge','Text'=>'Combine']),
        new DtoTab(['Slug'=>'Trash','Text'=>'Delete'),
    ];
    $selected = $_REQUEST['Tab'];// say, 'Trash',
    if (in_array($selected,DtoCfd::arrDto_column($arrTabs,'Slug')) {
        ....
    */
    public static function arrDto_column(array $dtoThatIsAnArray, $subKey): array
    { // inspired by php's array_column

        $arr = array_map(function (DtoCfd $Dto) use ($subKey) {
            if (!isset($Dto->$subKey)) {
                $class_vars = get_class_vars(get_class($Dto));
                $arrPrettyClassVarNames = [];
                foreach ($class_vars as $name => $value) {
                    $arrKnownMeta = ['exceptKeys', 'onlyKeys']; // expand later, if needed
                    if (!in_array($name, $arrKnownMeta)) {
                        $arrPrettyClassVarNames[] = $name;
                    }
                }
                $strPrettyClassVarName = implode(', ', $arrPrettyClassVarNames);
                \EtError::AssertTrue(0, 'error', __FILE__, __LINE__, " subKey($subKey) is not a property of " . get_class($Dto) . ". Try these valid options: $strPrettyClassVarName");

            }
            return $Dto->$subKey;
        }, $dtoThatIsAnArray);

        return $arr;
    }

    public static function BuildOutBlade_doesHave_XXX(array $Blade, string $NameOfVariable, $MaybeVariableValue)
    {

        //$setButEmpty = ($typeOfSubmittedValue == 'string') ? (strlen(trim($dangerousValue)) == 0) : 0; // Careful: empty('0') evaluates to true
        $doesHave_FirstName = !(is_null($MaybeVariableValue) || empty(trim($MaybeVariableValue))) || (strlen(trim($MaybeVariableValue)) > 0);
        $MaybeVariableValueNew = ($doesHave_FirstName) ? trim($MaybeVariableValue) : null;
        $doesHave_VarName = "doesHave_{$NameOfVariable}";
        #$Blade[$doesHave_VarName] = $doesHave_FirstName;
        $Blade[$NameOfVariable] = $MaybeVariableValueNew;

        return $Blade;

    }

    public function getValue_notHave(string $NameOfVariable, $DefaultVarValue_ifVarIsNull)
    {
        if (is_null($this->$NameOfVariable)) {
            return $DefaultVarValue_ifVarIsNull;
        } else {
            return $this->$NameOfVariable;
        }
    }

    public function __construct(array $asrSubmission,  $doAssumeValuesAreValid = false)
    {
        if ($doAssumeValuesAreValid) {
            foreach ($asrSubmission as $key=>$val) {
                $this->$key = $val;
            }
            return;
        }


        // $parameters = $asrSubmission;
        //        $class = new ReflectionClass(static::class);
        //        $properties = $this->getPublicProperties($class);
        //
        //
        //        foreach ($parameters as $key => $thisValue) {
        //
        //            // Use method with appropriate name to validate the value
        //            $validatingMethodName = "{$key}_Validates";
        //            if (method_exists($this, $validatingMethodName)) {
        //                $DtoValidation = $this->$validatingMethodName($thisValue);
        //                if (!$DtoValidation->isValid) {
        //                    if (is_array($thisValue)) {
        //                        $thisValue = var_export($thisValue);
        //                    }
        //                    throw DtoCfdError::NotValidating($DtoValidation, $key, $thisValue);
        //                }
        //            }
        //
        //            // if starts with doesHave or doesHave_, then ensure it is set, not empty, and true-ish. Otherwise, it must be null.  (feels right, maybe change in the future)
        //            $prefix = 'doesHave_';
        //
        //            if (static::StartsWith($prefix, $key)) {
        //
        //
        //                // FYI: This doesn't handle defaults yet - that would be nice.
        //                $other_property_name = static::TrimOffFront($prefix, $key);
        //                if (!isset($properties[$other_property_name])) {
        //                    // Missing as even an option
        //
        //                    throw DtoCfdError::doesHave_MissingFor_AccompanyingProperty($prefix, $other_property_name, $key);
        //                } else {
        //                    // There is a mathcing referenced property
        //                    // is it set?
        //                    //if (!isset($parameters[$other_property_name])) {
        //                    if (!key_exists($other_property_name, $parameters)) {
        //                        throw DtoCfdError::doesHave_ControlledProperty_notSet("The property '$other_property_name', which is referenced by '$key', MUST be set during construction.");
        //                    }
        //
        //                    // Ok, it is set.  Is it legit?
        //                    $controllingLogic = $parameters[$key];
        //
        //
        //                    $controlledProperty_value = $parameters[$other_property_name];
        //                    if ($controllingLogic == true) {
        //
        //                        if (is_null($parameters[$other_property_name])) {
        //                            // is null
        //                            $isOtherValueTrueish = false;
        //                            throw DtoCfdError::LogicError("The property '$other_property_name', which is referenced by '$key', MUST be set something other than null during construction.");
        ////                        //doesHave is true, so the th e property better be true-ish
        ////                        if ($controlledProperty_value == false) {
        ////                            // is false
        ////                            $isOtherValueTrueish = false;
        ////                        } elseif (is_null($controlledProperty_value)) {
        ////                            // is null
        ////                            $isOtherValueTrueish = false;
        ////                        } elseif (is_string($controlledProperty_value) && empty(trim($controlledProperty_value))) {
        ////                            // is '' or '  '
        ////                            $isOtherValueTrueish = false;
        //                        } else {
        //                            // OK
        //                            $isOtherValueTrueish = true;
        //                        }
        //
        //                        if ($isOtherValueTrueish) {
        //                            // OK
        //                        } else {
        //                            $t = gettype($controlledProperty_value);
        //                            return DtoCfdError::doesHave_NotTrueFor_AccompanyingProperty("Property '$other_property_name' is not true-ish, even though you have another property called $key and it is set to true.  This other value must be set to something that is not false-like. It is $t($controlledProperty_value)  ");
        //
        //                        }
        //                    } elseif ($controllingLogic == false) {
        //                        if (is_null($controlledProperty_value)) {
        //                            // ok
        //                        } else {
        //                            throw DtoCfdError::doesHave_NotFalseFor_AccompanyingProperty("When a controlling doesHave property($key) is false, the referenced property($other_property_name) must be set to null, but it was actually '$controlledProperty_value' ");
        //                        }
        //
        //                    } else {
        //                        throw DtoCfdError::LogicError('I can not program');
        //                    }
        //
        //
        //                }
        //            } else {
        //
        //
        //            }
        //
        //        }
        #parent::__construct($parameters);
        $dtoValid = static::preValidateSubmission($asrSubmission);
        if (!$dtoValid->isValid) {
            $errorPrefix = '';//get_called_class().": ";


            #\EtError::AssertTrue(0,'error',__FILE__,__LINE__,"{$dtoValid->enumReason} {$dtoValid->message}");
            if ($dtoValid->enumReason == 'doesHave_MissingFor_AccompanyingProperty') {
                throw DtoCfdError::doesHave_MissingFor_AccompanyingProperty($errorPrefix . $dtoValid->message);

            } else if ($dtoValid->enumReason == 'doesHave_ControlledProperty_notSet') {
                throw DtoCfdError::doesHave_ControlledProperty_notSet($errorPrefix . $dtoValid->message);

            } else if ($dtoValid->enumReason == 'doesHave_ControlledProperty_notSet') {
                throw DtoCfdError::doesHave_ControlledProperty_notSet($errorPrefix . $dtoValid->message);

            } else if ($dtoValid->enumReason == 'LogicError') {
                throw DtoCfdError::LogicError($errorPrefix . $dtoValid->message);
            } else if ($dtoValid->enumReason == 'reducedToEmpty') {
                throw DtoCfdError::LogicError($errorPrefix . "{$dtoValid->enumReason}: " . $dtoValid->message);
            } else if ($dtoValid->enumReason == 'doesHave_notSubmitted') {
                throw DtoCfdError::doesHave_ControlledProperty_notSet($errorPrefix . "{$dtoValid->enumReason}: " . $dtoValid->message);

            } else {
                throw DtoCfdError::LogicError($errorPrefix . "Default error for {$dtoValid->enumReason} " . $dtoValid->message);
            }
        }


        $objRef = new ReflectionClass($this);
        #$arrStaticProperties = $objRef->getProperties(ReflectionProperty::IS_STATIC);
//        print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."<pre>";
//        print_r($asrSubmission);
//        print "</pre>";


        foreach ($asrSubmission as $submittedPropertyName => $submittedValue) {
            //if ()
            $isStatic = $objRef->getProperty($submittedPropertyName)->isStatic() ? 1 : 0 ;

//            print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."<pre>";
//            print_r("---- $submittedPropertyName: ".$isStatic);
//            print "</pre>";
            if ($isStatic) {
                $this::$$submittedPropertyName = $submittedValue;
            } else {
                $this->$submittedPropertyName = $submittedValue;
            }

        }

    }


    public static function preValidateSubmission($asrSubmissionOrObject): DtoValueValidation
    {   /* Remember: All properties must be set to something, even if it is null
        */
        $asr_RichProperties = static::getCfdRichProperties();

//        if (get_called_class() == 'testworld\DtoDummy_hasReferencedProperty') {
//            print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."<pre>";
//print_r($asr_RichProperties);
//print_r(static::getRichProperties());
//print "</pre>";
//
//        }

        $asrRichProperties = $asr_RichProperties['properties'];

        // ----------------------------------- simple check ------------------------------------------------------------
        foreach ($asrSubmissionOrObject as $submittedPropertyName => $proposedValue) {
            $dtoValid = static::preValidateProperty($submittedPropertyName, $proposedValue, null);
            if (!$dtoValid->isValid) {
                return $dtoValid;
            }
        }
        unset($submittedPropertyName);

        // ----------------------------------- notExtras ---------------------------------------------------------------
        foreach ($asrSubmissionOrObject as $submittedPropertyName => $submittedValue) {
            if (!isset($asr_RichProperties['properties'][$submittedPropertyName])) {
                if (!property_exists(get_called_class(),$submittedPropertyName)){

                    return new DtoValueValidation(['isValid' => false, 'enumReason' => 'extraData', 'message' => " '$submittedPropertyName' is not an allowed property for '" . static::class . "'"]);

                }
            }
        }


        // ----------------------------------- doesHave_ 1 of 2 --------------------------------------------------------
        // if starts with 'doesHave' then make sure:
        //  - that the only two options are 'boolean' or 'boolean|null'
        //  - there is a correspdoning deferenced var
        //  - if set to a boolean (vs. null) that it matches the truth of the derefences var, mainly, true if
        //      the other one is not null, and false if the other one is null.
        //  - if it is set to null, then automatically set it to proper state
        // State: it will be set to semething (maybe null if that was specified as an option)

        $prefix = 'doesHave_';  // Must be specked to boolean, or boolean|auto
                                // If 'auto', then it is set automatically if unset, or set to null.
        foreach ($asrRichProperties as $speckedPropertyName => $proposedValue) {
            $asrRichProperty = $asrRichProperties[$speckedPropertyName];

            if (static::StartsWith($prefix, $speckedPropertyName)) {
                // meets spec 'boolean' or 'boolean|null'
                if (count(array_intersect(['boolean'], $asrRichProperty['types'])) != 1) {
                    return new DtoValueValidation(['isValid' => false, 'enumReason' => 'doesHave_NotSpeckedTo_Boolean', 'message' => " '$speckedPropertyName' must be specked to have type boolean (and maybe null): " . static::class . "'"]);
                }

                // State: this is a boolean, maybe with optional null.
                $doesThisHaveNullAsAnOption = $asrRichProperty['hasNullAsType'];
                $dereferenced_property_name = static::TrimOffFront($prefix, $speckedPropertyName);
                $is_dereferenced_property_name_specked_for_this_class = isset($asrRichProperties[$dereferenced_property_name]);

                // There must be a correcsponding property
                if (!$is_dereferenced_property_name_specked_for_this_class) {
                    return new DtoValueValidation(['isValid' => false, 'enumReason' => 'doesHave_dangles', 'message' => " '$speckedPropertyName' must reference another property, namely '$dereferenced_property_name' is missing: " . static::class . "'"]);
                }

                $amISet_ish = (isset($asrSubmissionOrObject[$speckedPropertyName]) && !is_null($asrSubmissionOrObject[$speckedPropertyName])) ? 1 : 0;
                $isDereferenceProperty_set_ish = (isset($asrSubmissionOrObject[$dereferenced_property_name]) && !is_null($asrSubmissionOrObject[$dereferenced_property_name])) ? 1 : 0;

                // - is the other var non-null?
                #$asrRichProperty_ofDereferencedProperty = $asrRichProperties[$dereferenced_property_name];
                #$isDereferenceProperty_setTo_null = is_null($asrSubmissionOrObject[$dereferenced_property_name]);
                // ensure I'm set unless I have null as an option.
                if (!$doesThisHaveNullAsAnOption) {
                    if (!$amISet_ish) {
                        return new DtoValueValidation(['isValid' => false, 'enumReason' => 'doesHave_notSet', 'message' => " '$speckedPropertyName' must be set to true or false, unless it is specked to have null as an option. '$dereferenced_property_name' is missing: " . static::class . "'"]);
                    }
                }

                // If I'm not set, then set me automatically.
                if (!$amISet_ish) {
                    $asrSubmissionOrObject[$speckedPropertyName] = ($isDereferenceProperty_set_ish) ? 1 : 0;
                }

                // Doublecheck that I am set properly
                $iDoesHave = $asrSubmissionOrObject[$speckedPropertyName];
                if ($amISet_ish != $isDereferenceProperty_set_ish) {
                    $isIsNotSet = $isDereferenceProperty_set_ish ? 'is set' : 'is not set';
                    return new DtoValueValidation(['isValid' => false, 'enumReason' => 'doesHave_doesNotMatch', 'message' => " '$speckedPropertyName($iDoesHave)' references '$dereferenced_property_name', but '$dereferenced_property_name' $isIsNotSet: " . static::class . "'"]);
                }
//                if ('doesHave_PrettyVersion' == $speckedPropertyName) {
//                    print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."
//                    asrSubmissionOrObject[$speckedPropertyName]({$asrSubmissionOrObject[$speckedPropertyName]})
//                    $speckedPropertyName($amISet_ish) $dereferenced_property_name($isDereferenceProperty_set_ish)
//<pre>";
//print_r($asrRichProperties);
//print_r($asrSubmissionOrObject);
//print "</pre>";
//exit;
//
//                }


//                if ($isDereferenceProperty_setTo_null) {
//                    // if it is null, automatically make it reference the status of the other one.
//                    $asrSubmissionOrObject[$speckedPropertyName] = ($isDereferenceProperty_setTo_null) ? false : true;
//
//                } else {
//                    // if that other property isn't null, does it match the status of the other one?
//                    $speckedProperty_nonNullValue = $asrSubmissionOrObject[$speckedPropertyName];
//                    if ($speckedProperty_nonNullValue == $isDereferenceProperty_setTo_null){
//                        $isDereferenceProperty_setTo_null_tf = $isDereferenceProperty_setTo_null ? 'null' : 'not null';
//                        $speckedProperty_nonNullValue_tf = $speckedProperty_nonNullValue ? 'True' : 'False';
//                        return new DtoValueValidation(['isValid' => false, 'enumReason' => 'doesHave_setButNotMatchingReality', 'message' => " '$speckedPropertyName' is $speckedProperty_nonNullValue_tf, but $dereferenced_property_name' is $isDereferenceProperty_setTo_null_tf: " . static::class . "'"]);
//                    }
//                }


            }
        }


        //  Hints on how to quickly use, or not, a value if null for not-set.
        //            $result = null ? 0 : 1; // 1
        //            $result = null ?: 1;    // 1
        //            $result = 0 ?: 1;       // 1
        //            $result = $asrRichProperties['abcd'] ?: 1; // Notice: Undefined index: abcd
        //            if($Dto->email ?? false) {
        //
        //            $result = $asrRichProperties['Title']['isMeta']     ?? 1;   // 0 <-- this is what isMeta is set to
        //            $result = $asrRichProperties['Title']['abcd']       ?? 1;   // 1 <--- abcd is not an array element
        //            $a = 'hw';
        //            $result = $a       ?? 2;   // hw
        //            $a = true;
        //            $result = $a       ?? 3;   // 1 <-- I'm surprised by this one.  I think true is printing as '1' now.
        //            $a = false;
        //            $result = $a       ?? 3;   // '' <--   I think true is printing as '1' now, but false is ''.
        //            $a = '';
        //            $result = $a       ?? 3;   // ''
        //            $a = 0;
        //            $result = $a       ?? 3;   // 0
        //            unset($a);
        //            $result = $a       ?? 3;   // 3
        //            print "result: ". $result;
        //            exit;

//        // If we have a a doesHave_ property, and it isn't set and is optional,
//        //      then lets ensure it gets automatically set
//        //      (don't fail if it isn't set at construction)
//        //      (If it is set at construction, make sure it is legit)
//
//        $prefix = 'doesHave_';
//        foreach ($asrRichProperties as $speckedPropertyName => $proposedValue) {
//             $asrRichProperty = $asrRichProperties[$speckedPropertyName];
//
//            if (static::StartsWith($prefix, $speckedPropertyName)) {
//                // FYI: This doesn't handle defaults yet - that would be nice.
//                $dereferenced_property_name = static::TrimOffFront($prefix, $speckedPropertyName);
//
//                if (!isset($asrRichProperties[$dereferenced_property_name])) {
//                    // Missing as even an optionx
//                    return new DtoValueValidation([
//                        'isValid' => false,
//                        'enumReason' => 'doesHave_MissingFor_AccompanyingProperty',
//                        'message' => "doesHave_MissingFor_AccompanyingProperty $dereferenced_property_name $speckedPropertyName"
//                    ]);
//                    #throw DtoCfdError::doesHave_MissingFor_AccompanyingProperty($prefix, $dereferenced_property_name, $speckedPropertyName);
//                } else {
//                    // There is a mathcing referenced property
//                    // is it set?
//                    //if (!isset($parameters[$dereferenced_property_name])) {
//                    $asrRichProperty_ofDereferencedProperty = $asrRichProperties[$dereferenced_property_name];
//
//                    // State: $speckedPropertyName exists and controls a property called $dereferenced_property_name
//
//                    // is the $dereferenced_property_name even a thing? It has better be a thing.
//
//                    // if $speckedPropertyName is set, then ensure to matches $dereferenced_property_name otherwise, set it
//
//
//
//                    $isDereferencePropertyRequired = $asrRichProperty_ofDereferencedProperty['isRequired'];
//                    $isDereferencedPropertySet = key_exists($dereferenced_property_name, $asrSubmissionOrObject);
//                    if ($isDereferencePropertyRequired) {
//                        if (!$isDereferencedPropertySet) {
//                            return new DtoValueValidation([
//                                'isValid' => false,
//                                'enumReason' => 'doesHave_ControlledProperty_notSet',
//                                'message' => "The property '$dereferenced_property_name', which is referenced by '$speckedPropertyName', MUST be set during construction when '$dereferenced_property_name' is required."
//                            ]);
//                            #throw DtoCfdError::doesHave_ControlledProperty_notSet("The property '$dereferenced_property_name', which is referenced by '$speckedPropertyName', MUST be set during construction.");
//                        }
//                    }
//
//                    $isControllingPropertySet = key_exists($speckedPropertyName, $asrSubmissionOrObject);
//
//
//                    if (!$isControllingPropertySet) {
//                        $asrSubmissionOrObject[$speckedPropertyName] = is_null($asrSubmissionOrObject[$dereferenced_property_name]) ? 0 : 1;
//                    }
//
//
//                    // Ok, it is set.  Is it legit?
//                    if (!isset($asrSubmissionOrObject[$speckedPropertyName])) {
//                        print "<br>" . __FILE__ . __LINE__ . __METHOD__ . "<br>" . "speckedPropertyName($speckedPropertyName)<pre>";
//                        print_r($asrSubmissionOrObject);
//                        print "</pre>";
//                        \EtError::AssertTrue(0, 'error', __FILE__, __LINE__, "wth*ck");
//                    }
//
//                    $controllingLogic = $asrSubmissionOrObject[$speckedPropertyName];
//
//
//                    $controlledProperty_value = $asrSubmissionOrObject[$dereferenced_property_name];
//                    if ($controllingLogic == true) {
//
//                        if (is_null($asrSubmissionOrObject[$dereferenced_property_name])) {
//                            // is null
//                            $isOtherValueTrueish = false;
//                            return new DtoValueValidation([
//                                'isValid' => false,
//                                'enumReason' => 'LogicError',
//                                'message' => "The property '$dereferenced_property_name', which is referenced by '$speckedPropertyName', MUST be set something other than null during construction."
//                            ]);
//                            #throw DtoCfdError::LogicError("The property '$dereferenced_property_name', which is referenced by '$speckedPropertyName', MUST be set something other than null during construction.");
//
//                        } else {
//                            // OK
//                            $isOtherValueTrueish = true;
//                        }
//
//                        if ($isOtherValueTrueish) {
//                            // OK
//                        } else {
//                            $t = gettype($controlledProperty_value);
//                            return new DtoValueValidation([
//                                'isValid' => false,
//                                'enumReason' => 'doesHave_NotTrueFor_AccompanyingProperty',
//                                'message' => "Property '$dereferenced_property_name' is not true-ish, even though you have another property called $speckedPropertyName and it is set to true.  This other value must be set to something that is not false-like. It is $t($controlledProperty_value)  "
//                            ]);
//                            #return DtoCfdError::doesHave_NotTrueFor_AccompanyingProperty("Property '$dereferenced_property_name' is not true-ish, even though you have another property called $speckedPropertyName and it is set to true.  This other value must be set to something that is not false-like. It is $t($controlledProperty_value)  ");
//
//                        }
//                    } elseif ($controllingLogic == false) {
//                        if (is_null($controlledProperty_value)) {
//                            // ok
//                        } else {
//                            return new DtoValueValidation([
//                                'isValid' => false,
//                                'enumReason' => 'doesHave_NotFalseFor_AccompanyingProperty',
//                                'message' => "When a controlling doesHave property($speckedPropertyName) is false, the referenced property($dereferenced_property_name) must be set to null, but it was actually '$controlledProperty_value' "
//                            ]);
//                            #throw DtoCfdError::doesHave_NotFalseFor_AccompanyingProperty("When a controlling doesHave property($speckedPropertyName) is false, the referenced property($dereferenced_property_name) must be set to null, but it was actually '$controlledProperty_value' ");
//                        }
//
//                    } else {
//                        return new DtoValueValidation([
//                            'isValid' => false,
//                            'enumReason' => 'LogicError',
//                            'message' => "I can not program for speckedPropertyName($speckedPropertyName)"
//                        ]);
//                        #throw DtoCfdError::LogicError('I can not program');
//                    }
//
//
//                }
//            } else {
//
//
//            }
//        }

        return new DtoValueValidation(['isValid' => true]);
    }

    public
    static function preValidateLoneValue($dangerousValue, ?bool $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull, array $callingRichProperties): DtoValueValidation
    {
        $asr_RichProperties = static::getCfdRichProperties();

        if (count($asr_RichProperties['properties']) != 1) {
            print "<br>".__FILE__.__LINE__.__METHOD__."<br>"."<pre>";
print_r($asr_RichProperties);
print "</pre>";

            return new DtoValueValidation(['isValid' => false, 'enumReason' => 'onlyLoneSubValuesAllowed', 'message' => get_called_class() . ": Composed DTOs can only have a single value unless they are subclasses of CFDs, like DtoEmail just has a single value for its email address. " . __LINE__]);
        }
        $propertyName = array_key_first($asr_RichProperties['properties']);
        return static::preValidateProperty($propertyName, $dangerousValue, $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull);
    }

    public
    static function preValidateProperty($propertyName, $dangerousValue, ?bool $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull): DtoValueValidation
    {
        $asr_RichProperties = static::getRichProperties();
        $asrRichProperties = $asr_RichProperties['properties'];
        $asrMeta = $asr_RichProperties['_meta'];
        $isAProperty = isset($asrRichProperties[$propertyName]);
        if (!$isAProperty) {
            return new DtoValueValidation(['isValid' => false, 'enumReason' => 'notARealOption', 'message' => "'$propertyName' is not a valid property for class " . get_called_class().". Hint: Sometimes you see this when you pass a list instead of associative array"]);
        }


        $isNonMeta = !$asrRichProperties[$propertyName]['isMeta'];

        $isRequired = $asrRichProperties[$propertyName]['isRequired'] || $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull;


        $typeOfSubmittedValue = gettype($dangerousValue);
        $typeOfSubmittedValue = ($typeOfSubmittedValue == 'NULL') ? 'null' : $typeOfSubmittedValue;
       # $typeOfSubmittedValue = $typeOfSubmittedValue;

        $isDictatingRequiredState_zo = is_null($isRequired_ifSet_otherwise_requireAsConfiggedWhenNull) ? 0 : 1;
        $isRequired_orig_zo = $asrRichProperties[$propertyName]['isRequired'] ? 1 : 0;
        if (!$isDictatingRequiredState_zo) {
            $isRequired_new_zo = $isRequired_orig_zo;
        } else {
            $isRequired_new_zo = $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull;
        }
        $isRequired_zo = ($isDictatingRequiredState_zo && ($isRequired_orig_zo != $isRequired_new_zo)) ? 1 : 0;
        $didRequiredStateChange_zo = ($isRequired_zo != $isRequired_orig_zo) ? 1 : 0;

        $asrRichProperties[$propertyName]['isDictatingRequiredState_zo'] = $isDictatingRequiredState_zo;
        $asrRichProperties[$propertyName]['isRequired_orig_zo'] = $isRequired_orig_zo;
        $asrRichProperties[$propertyName]['isRequired_new_zo'] = $isRequired_new_zo;
        $asrRichProperties[$propertyName]['isRequired_zo'] = $isRequired_zo;
        $asrRichProperties[$propertyName]['didRequiredStateChange_zo'] = $didRequiredStateChange_zo;
        $asrRichProperties[$propertyName]['isRequired'] = $isRequired_zo;


        $setButEmpty = ($typeOfSubmittedValue == 'string') ? (strlen(trim($dangerousValue)) == 0) : 0; //  Careful: empty('0') evaluates to true


        $nonEmpty = !$setButEmpty;
        if ($isRequired) {
            $isSetEnough = !is_null($dangerousValue);
            if (!$isSetEnough) {
                return new DtoValueValidation(['isValid' => false, 'enumReason' => 'setToNullWhenRequired', 'message' => "'$propertyName' is null but it is required to be set to a non-null value. for (" . get_called_class() . ")" . __LINE__]);
            }


            // 1/3/20' An empty string is still a string.  If you want something else, make a custom Cfd
            //            // if string, ensure not empty (we're not that easily fooled)
            //            if ($typeOfSubmittedValue == 'string' && is_string($dangerousValue)) {
            //                if ($setButEmpty) {
            //                    return new DtoValueValidation(['isValid' => false, 'enumReason' => 'reducedToEmpty', 'message' => "'$propertyName' is a string but it reduces to nothing which is not ok when the property is required, which it is for " . static::class . ". FYI: original value is ($dangerousValue)"]);
            //                }
            //            }
            #print "<br>-XXXXX $propertyName($dangerousValue / $isRequired_ifSet_otherwise_requireAsConfiggedWhenNull) typeOfBe($typeOfBe)";
        }

        // ensure matching type
        // If this is another DtoCfd, run validation on it
        #foreach ($asrRichProperties[$propertyName]['types'] as $nameOfOneOfTheAllowedTypes_willNotBeNull) {
        $typeItShouldBe_Name = $asrRichProperties[$propertyName]['type'];
        if (is_subclass_of($typeItShouldBe_Name, self::class)) {
            #if (method_exists($typeItShouldBe_Name, 'preValidateLoneValue')) {

            // FYI: overriding the isREquired logic as a big whacked to catch unset here.

            $smallerDangerousValue = ($typeOfSubmittedValue == 'string') ? trim($dangerousValue) : $dangerousValue;
            $isSet_ish = empty($smallerDangerousValue) ? 0 : 1;

            if ($isRequired_zo || $isSet_ish) {
                #return new DtoValueValidation(['isValid'=>false, 'enumReason'=>'tmp', 'message'=>"Validating: isRequired_zo($isRequired_zo) isSet_ish($isSet_ish) smallerDangerousValue($smallerDangerousValue)"]);

                $submittedValueIsChildOfCfd = ($typeOfSubmittedValue == 'object' && is_subclass_of($dangerousValue, self::class)) ? 1 : 0;
                $isValueObject = ($typeOfSubmittedValue == 'object') ? 1 : 0;


                if ($isValueObject) {
                    $dangerousValueClassType_weKnowIsObj = get_class($dangerousValue);
                    $isCompatibleClass = in_array($dangerousValueClassType_weKnowIsObj, $asrRichProperties[$propertyName]['types']);
                    if (!$isCompatibleClass) {
                        $allowedClassTypes = implode(', ', $asrRichProperties[$propertyName]['types']);
                        $thisClassName = get_called_class();
                        return new DtoValueValidation(['isValid' => false, 'enumReason' => 'typeMismatch', 'message' => "In '$thisClassName', the property '$propertyName' should be an object of type ($allowedClassTypes), but it was set to '$dangerousValueClassType_weKnowIsObj'"]);
                    }

                } else {
                    // I'm not positive of this logic... 12/19'
                    $DtoValidation = $asrRichProperties[$propertyName]['type']::preValidateLoneValue($dangerousValue, $isRequired_zo, $asr_RichProperties);
                    if (!$DtoValidation->isValid) {
                        return $DtoValidation;
                    }
                }

            } else {
                #return new DtoValueValidation(['isValid'=>false, 'enumReason'=>'tmp', 'message'=>"Skipping: isRequired_zo($isRequired_zo) isSet_ish($isSet_ish) smallerDangerousValue($smallerDangerousValue)"]);
            }
        } else {
            # - you left off on how to pass a string to Email, which should be allowed
            $doesTypeMatch = !$asrRichProperties[$propertyName]['hasDtoDocComment'] || ($asrRichProperties[$propertyName]['hasNullAsType'] && ($typeOfSubmittedValue == 'null')) || in_array($typeOfSubmittedValue, $asrRichProperties[$propertyName]['types']);
            if (!$doesTypeMatch) {
                if ($typeOfSubmittedValue == 'object') {
                    $typeOfSubmittedValue = get_class($dangerousValue);
                }
                return new DtoValueValidation(['isValid' => false, 'enumReason' => 'wrongType', 'message' => "'$propertyName' must be of a type (" . implode(',', $asrRichProperties[$propertyName]['types']) . ") but received '$typeOfSubmittedValue'. " . __LINE__]);
            }
        }

        // ensure validates by custom function, if there
        $validatingMethodName = "{$propertyName}_Validates";
        if ($nonEmpty && method_exists(static::class, $validatingMethodName)) {
            $DtoValidation = static::$validatingMethodName($dangerousValue);
            if (!$DtoValidation->isValid) {

                #&& in_array($DtoValidation->enumReason, ['setToNullWhenRequired', 'reducedToEmpty']) ) {
                return $DtoValidation;
            }
        }

        return new DtoValueValidation(['isValid' => true, 'enumReason' => 'ok']);
    }

    public function getTerseRichCfdProperties_withValues() : array {
        $asrRichPropreties = static::getRichProperties();
        $asrCfdProperties = [];
        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProprety) {
            if ($asrRichProprety['hasADocComment']) {
                $asrCfdProperties[$propertyName] = [
                    'shortPropertyName '=> $propertyName,
                    'isNull'=> is_null($this->$propertyName),
                    'Value'=> $this->$propertyName
                    ];
            }
        }
        return $asrCfdProperties;
    }


    public
    static function getCfdRichProperties(): array
    {
        $asrRichPropreties =  static::getRichProperties_ofDtoNamed(static::class);

        foreach ($asrRichPropreties['properties'] as $propertyName=>$asrRichProprety) {
            if ($asrRichProprety['hasDtoDocComment']) {
            } else {
                unset($asrRichPropreties['properties'][$propertyName]);
                // Meta is probably wrong now
            }
        }

        return $asrRichPropreties;
    }


    public
    static function getRichProperties(): array
    {
        return static::getRichProperties_ofDtoNamed(static::class);
    }

    private
    static function getRichProperties_ofDtoNamed(string $DtoCfdClassName): array
    {
        $class = new ReflectionClass($DtoCfdClassName);
        $richProperties = [];



        $numNonMeta = 0;
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty) {
            // isRequired
            // isMeta
            // hasDefault (tbd)
            $propertyName = $reflectionProperty->getName();





            $richProperties[$propertyName] = [];
            $richProperties[$propertyName]['name'] = $propertyName;

            // Is Meta
            $richProperties[$propertyName]['isMeta'] = 0;
            foreach (static::META_PREFIXES as $metaPrefix) {
                if (strpos($propertyName, $metaPrefix) === 0) {
                    $richProperties[$propertyName]['isMeta'] = 1;
                    break;
                }
            }

            if ($richProperties[$propertyName]['isMeta'] == false) {
                foreach (static::META_PROPERTY_NAMES as $metaPropertyName) {
                    if ($metaPropertyName == $propertyName) {
                        $richProperties[$propertyName]['isMeta'] = 1;
                    }
                }
            }
            $numNonMeta = $numNonMeta + !$richProperties[$propertyName]['isMeta'];
            #$richProperties['numNonMeta']=$numNonMeta;

            // Handle default values
            $doesHaveDefaultValueSet =  isset($DtoCfdClassName::$$propertyName);
            if ($doesHaveDefaultValueSet) {
                $richProperties[$propertyName]['hasStaticDefault'] = 1;
                $richProperties[$propertyName]['staticDefault'] = $DtoCfdClassName::$$propertyName;
            } else {
                $richProperties[$propertyName]['hasStaticDefault'] = 0;
                $richProperties[$propertyName]['staticDefault'] = null;
            }

            // Is Required
            $docCommentForThisProperty = trim($reflectionProperty->getDocComment());
            $hasDocComment = $docCommentForThisProperty;
            $richProperties[$propertyName]['hasADocComment'] = $hasDocComment ? 1 : 0;
            $richProperties[$propertyName]['docCommentForThisProperty'] = $docCommentForThisProperty;


            preg_match('/\@var ((?:(?:[\w|\\\\])+(?:\[\])?)+)/', $docCommentForThisProperty, $matches);
            // from Spatie

            $hasAtVarDocComment = (count($matches));
            $richProperties[$propertyName]['hasDtoDocComment'] = $hasAtVarDocComment ? 1 : 0;


            $varDocComment = end($matches);
            $richProperties[$propertyName]['dtoDocComment'] = $varDocComment;
            $arrTypes = array_map('trim', explode('|', $varDocComment));// explode('|', $varDocComment);
            $arrTypes_withoutBrackets = str_replace('[]', '', $arrTypes); // nix me


            $hasNullAsType = (in_array('null', $arrTypes)) ? 1 : 0;
            $richProperties[$propertyName]['hasNullAsType'] = $hasNullAsType ? 1 : 0;
            $richProperties[$propertyName]['isRequired'] = ($hasDocComment && !$hasNullAsType) ? 1 : 0;

            if (!$varDocComment) {
                $richProperties[$propertyName]['type'] = '?';
                $richProperties[$propertyName]['types'] = ['?'];
            } else {
                $slot_or_false = array_search('null', $arrTypes_withoutBrackets);
                if ($slot_or_false !== false) {
                    unset($arrTypes_withoutBrackets[$slot_or_false]);
                }
                $arrKeys = array_keys($arrTypes_withoutBrackets);
                $firstKey = $arrKeys[0];

                $first = $arrTypes_withoutBrackets[$firstKey];


                $richProperties[$propertyName]['type'] = $first;
                $richProperties[$propertyName]['types'] = $arrTypes_withoutBrackets;
            }

            // Ensure no forbidden types
            $forbiddenTypes = $DtoCfdClassName::FORBIDDEN_TYPES;
            $forbiddenTypesFoundHere = array_intersect( $forbiddenTypes, $richProperties[$propertyName]['types']);
            if (count($forbiddenTypesFoundHere) > 0) {
                $csvTypes = implode(', ', $forbiddenTypesFoundHere);
                throw DtoCfdError::LogicError("This ".get_called_class()."($propertyName) is specked to be of type $csvTypes, but that is on the forbidden list." );
            }


//             // hmm - not flexible enouhg
//             if ($richProperties[$propertyName]['type'] == 'string') {
//                 $richProperties[$propertyName]['input_type'] = 'text';
//             } else {
//                $richProperties[$propertyName]['input_type'] = 'no input scheme set for ['.$richProperties[$propertyName]['type'].']';
//             }





        }


        $asr = ['_meta' =>
            ['numNonMeta' => $numNonMeta,
                'className'=>get_called_class()],
            'properties' => $richProperties];

        return $asr;

    }
}

//                } else {
//                    // So, we have an the 'other' property, but is it kosher?
//                    if ($thisValue == true) {
//                        $otherValue = $parameters[$other_property_name];
//                        throw DtoCfdError::doesHave_NotTrueFor_AccompanyingProperty($prefix, $other_property_name, $key, $otherValue);
//                    } else {
//                        throw DtoCfdError::LogicError('not handles yet', 'not handles yet', 'not handles yet');
//                    }
//
//
//
//                    $isOtherValueTrueish = null;
//                    $isOtherValueSet = null;
//                    if (!isset($parameters[$other_property_name])) {
//                        $isOtherValueTrueish = false;
//
//                    } else {
//                        $otherValue = $parameters[$other_property_name];
//
//                        if ($otherValue == false) {
//                            // is false
//                            $isOtherValueTrueish = false;
//                        } elseif (is_null($otherValue)) {
//                            // is null
//                            $isOtherValueTrueish = false;
//                        } elseif (is_string($otherValue) && empty(trim($otherValue))) {
//                            // is '' or '  '
//                            $isOtherValueTrueish = false;
//                        } else {
//                            // OK
//                            $isOtherValueTrueish = true;
//                        }
//                    } // output $isOtherValueTrueish
//
//                    if ($thisValue == true) {
//                        if ($isOtherValueTrueish) {
//                            // OK
//                        } else {
//                            $otherValue = $parameters[$other_property_name];
//                            throw DtoCfdError::doesHave_NotTrueFor_AccompanyingProperty($prefix, $other_property_name, $key, $otherValue);
//                        }
//                    } elseif ($thisValue == false) {
//                        // Other must be false. So, if default that isn't false-sish, then param be set to something false-ish
//                        // We might make this more thorough in the future
//                        if ($isOtherValueTrueish) {
//                            // Error - it must be false-ish
//                            if (isset( $parameters[$other_property_name])) {
//                                $otherValue = $properties[$other_property_name];
//                            } else {
//                                $otherValue = 'it was not set';
//                            }
//
//                            throw DtoCfdError::doesHave_NotFalseFor_AccompanyingProperty($prefix, $other_property_name, $key, $otherValue);
//                        } else {
//                            // Maybe
////                            $isThisPropertyOptional = xyz;
////                            if (!$isThisPropertyOptional && // if not optional, and it is set, it had better evaluate to false.
////
////                            if ($isThisPropertyOptional) {
////                            } elseif (!$isThisPropertyOptional) {
////                                if ($isOtherValueTrueish) {
////                                    throw DtoCfdError::doesHave_NotFalseFor_AccompanyingProperty($prefix, $property_name, $key);
////                                } else { // Not True-ish
////                                    // OK
////                                }
////                            } else {
////                                throw DtoCfdError::LogicError($prefix, $property_name, $key);
////                            }
//

//}

abstract class DtoCfd_DbRow extends DtoCfd
{
    //abstract $Uuid; // we'd do this if the language allowed it.
}


class DtoCfdError extends TypeError
{
    public static function NotValidating(DtoValueValidation $dtoValueValidation, string $property_name, $property_value): DtoCfdError
    {
        return new self("{$dtoValueValidation->message}");
    }

    public static function doesHave_MissingFor_AccompanyingProperty(string $prefix, string $property_name, string $needy_proerty_name): DtoCfdError
    {
        return new self("Property '$property_name' is missing, even though you have another property called $needy_proerty_name.");
    }

    public static function doesHave_ControlledProperty_notSet(string $msg): DtoCfdError
    {
        return new self($msg);
    }

    public static function doesHave_NotFalseFor_AccompanyingProperty(string $msg): DtoCfdError
    {
        return new self($msg);
    }

    public static function doesHave_NotTrueFor_AccompanyingProperty(string $msg): DtoCfdError
    {
        return new self($msg);
    }

    public static function ControllerNotBool(string $prefix, string $property_name, string $key, array $parameters, string $needy_proerty_name): DtoCfdError
    {
        $t = gettype($parameters[$key]);
        $value = $parameters[$key];
        return new self("Your controller field '$needy_proerty_name' must be true or false - nothing else. $t($value)");
    }

    public static function LogicError(string $message): DtoCfdError
    {

        return new self("I don't know how to program: $message");
    }
}



